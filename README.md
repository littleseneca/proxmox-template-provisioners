# Proxmox Template Provisioners
To use this tool, make sure that you have ssh-public-key auth to the root user of your proxmox host.
Then, set the host device in the inventory file to the name of your proxmox host. 

## CentOS 8 Stream Template
To deploy this template, simply run:
~~~
ansible-playbook template-centos8.yml
~~~
## Ubuntu 22.04 Template
To deploy this template, simply run:
~~~
ansible-playbook template-ubuntu2204.yml
~~~
## AlmaLinux 8 Template
Todeploy this template, simply run:
~~~
ansible-playbook template-alma8.yml
